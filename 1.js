// Si es lugar de meter cada opcion a mano lo tenemos en un array o nos viene de una base de datos
let poblaciones = ["Alicante", "Madrid", "Sevilla", "Santander"];
let values = ["AL", "MA", "SE", "SA"];

// Creamos la variable poblacion para seleccionar la etiqueta select
let poblacion = document.querySelector("select");

// Iteramos para crear cada opción en la lista desplegable
for (let i = 0; i < poblaciones.length; i++) {

    // Creamos la etiqueta option
    const option = document.createElement('option');
    //Le asignamos el value
    option.value = values[i];
    //Le asignamos el texto a mostrar
    option.text = poblaciones[i];
    // Inserta el elemento
    poblacion.append(option);

}

